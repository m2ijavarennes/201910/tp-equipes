import { Component, OnInit, Input } from '@angular/core';
import {EntrepriseService} from '../services/entreprise.service';
import {Personne} from '../model/personne';
import {NgForm} from'@angular/forms';
@Component({
  selector: 'app-equipe',
  templateUrl: './equipe.component.html',
  styleUrls: ['./equipe.component.scss']
})
export class EquipeComponent implements OnInit {
@Input() equipe:any;
@Input() i:number;
personnes:Personne[];
personneSelected:any;

  constructor(private entrepriseService:EntrepriseService) { }

  ngOnInit() {
  	this.personnes =this.entrepriseService.entreprise.personnes;
  }
  onEnleverEquipe(){
  	this.entrepriseService.enleverEquipe(this.i);
  }
  enleverPersonne(i){
  	this.entrepriseService.enleverPersonneEquipe(this.i,i);
  }
  isNotInTeam(p:Personne){
    let test = true;
    let id = p.id;
    // parcours les personnes de l'equipe
    for(let pers of this.equipe.personnes){
      if (id == pers.id){
        test = false;
      }
    }
    return test;
  }
  getListePersonne():Personne[]{
    let liste:Personne[] =[];
    for (let p of this.personnes){
      // si p n'est pas deja ds l 'equipe (equipe.personnes)
      if (this.isNotInTeam(p)){
        liste.push(p);
      }
    }
    return liste;
  }
  onAjouterPersonne(form:NgForm){
  	//console.log(form.value["personne"]);
    this.entrepriseService.ajouterPersonneEquipe(this.equipe,form.value["personne"]);
  }

}

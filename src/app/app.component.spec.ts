import { TestBed, async } from '@angular/core/testing';
//import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { EquipeComponent } from './equipe/equipe.component';
import { Personne } from './model/personne';
import { EntrepriseService } from './services/entreprise.service';


/* VM : tests créés par défaut lors de la génération du projet */
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, EquipeComponent
      ],
      /* le code qui suit est nécessaire au 
        fonctionnement de ngModel lors du test */
      imports: [ 
        FormsModule
      ],
      schemas: [
  //      NO_ERRORS_SCHEMA
      ],
      providers: [ EntrepriseService ]
      /* fin de l'ajout */
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'TP-Equipes'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('TP-Equipes');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('TP-Equipes app is running!');
  });
});

/***************************************
 *  Tests développés par l'équipe POEC *
 * *************************************/

describe('Structure de base de AppComponent', () => {

  beforeEach( () => {
    // code à exécuter avant chaque test unitaire
  });

  it('devrait avoir XXX');

});
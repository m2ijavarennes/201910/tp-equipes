import { Component,OnInit } from '@angular/core';
import { EntrepriseService } from './services/entreprise.service';
import { Entreprise} from './model/entreprise';
import { NgForm } from '@angular/forms';
import {Personne } from './model/personne';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // élément testé par Karma
  title = 'TP-Equipes';

  entreprise:Entreprise;
  nomEquipe:string;
  defaultEquipe:number =0;
  constructor (private entrepriseService:EntrepriseService){}
  ngOnInit(){
    this.entreprise = this.entrepriseService.entreprise;
  }
  onAjouterEquipe(){
    console.log('onAjouterEquipe()');
    this.entrepriseService.ajouterEquipe(this.nomEquipe);
  	this.nomEquipe =''; // je vide le champ input
  }
  onAjouterPersonne(form:NgForm){
    let idPersonne =this.entrepriseService.getPersonneNextId();
    let p = new Personne (idPersonne,form.value["prenom"],form.value["nom"]);
    this.entrepriseService.ajouterPersonne(p);
    // ajouter la personne ds 1 equipe
    let equipe = form.value["equipe"];
    console.log(equipe);
    if(equipe != 0 && equipe != undefined){
      this.entrepriseService.ajouterPersonneEquipe(form.value["equipe"],p);
      // ajouter ds l Equipe
    }
    form.reset();
  //  console.log(form.controls['equipe']);
  //  form.controls['equipe'].setValue(0);
  }
  onEnleverPersonne(i){
     this.entrepriseService.enleverPersonneEntreprise(i);
  }
}

/* Bibliothèque d'utilitaires */
export class Utils {
    /** Fonction mettant la première lettre d'une chaîne en majuscule */
    static capitalize(word) {
        return word[0].toUpperCase() + word.substr(1);
    }

}
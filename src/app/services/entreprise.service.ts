import {Personne} from '../model/personne';
import {Equipe} from '../model/equipe';
import {Entreprise} from '../model/entreprise';

export class EntrepriseService{
	entreprise:Entreprise=new Entreprise();

	ajouterEquipe(nom:string){
		console.log('ajouterEquipe()');
		let idEquipe = 1;
		let dernier = this.entreprise.equipes.length -1;
		if (dernier != -1){
			idEquipe = this.entreprise.equipes[dernier].id;
			idEquipe++;
		}
		console.log('ajouterEquipe()2');
		let e = new Equipe(idEquipe,nom);
		this.entreprise.equipes.push(e);
		console.log('ajouterEquipe()3');
	}
	enleverEquipe(i:number){
		this.entreprise.equipes.splice(i,1);
	}
	getPersonneNextId():number{
		let idPersonne = 1;
		let dernier = this.entreprise.personnes.length -1;
		if (dernier != -1){
			idPersonne = this.entreprise.personnes[dernier].id;
			idPersonne++;
		}
		return idPersonne;
	}
	ajouterPersonne(p:Personne){
		this.entreprise.personnes.push(p);
	}
	ajouterPersonneEquipe(e:Equipe,p:Personne){
		e.personnes.push(p);
	}
	enleverPersonneEquipe(iEquipe:number,iPersonne:number){
		this.entreprise.equipes[iEquipe].personnes.splice(iPersonne,1);
	}
	enleverPersonneEntreprise(i:number){
		let ok  = confirm('Voulez vous vraiment le faire ?');
		if (ok){
			let id= this.entreprise.personnes[i].id;
			let flag = true;
			for (let e of this.entreprise.equipes){
				for ( let p of e.personnes){
					if ( id == p.id){
						flag = false;
						break;
					}
				}
			}
			if (flag){
				this.entreprise.personnes.splice(i,1);
			}else{
				alert ('Cette personne est déjà ds 1 equipe');
			}
		}
	}
}